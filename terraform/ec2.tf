resource "aws_instance" "bastion" {
  ami = data.aws_ami.amazon_linux.id
  instance_type = var.instance_type
  subnet_id = element(module.vpc.public_subnets, 0)
  key_name = var.sshpubkey_file
  security_groups = [aws_security_group.packer_build.id]
  tags = {
    Name = "BastionHost_Amazon_Linux"
    "Network_Type"="Public"
    "Environment"="Development"
    "Author"="kiransgonella"

  }
}

resource "aws_instance" "web" {
  ami = data.aws_ami.amazon_linux.id
  instance_type = var.instance_type
  subnet_id = element(module.vpc.private_subnets, 0)
  key_name = var.sshpubkey_file
  security_groups = [aws_security_group.BastionHost_WebServer.id]
  tags = {
    Name = "WebServer_Amazon_Linux"
    Network_Type="Private"
    Environment="Development"
    "Author"="kiransgonella"
  }
}
